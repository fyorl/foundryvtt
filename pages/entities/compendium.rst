..	_compendium:

Compendium Packs
****************

Compendium packs provide content creators and game-masters with an amazing way to store and organize their content. The following video
provides an overview of the Compendium system and it's basic usage.

.. raw:: html

    <iframe width="640" height="360" src="https://www.youtube.com/embed/JMWPjX0QZkA" frameborder="0" allow="autoplay;
     encrypted-media" allowfullscreen style="margin-bottom:1em;"></iframe>
